# Simple Currency App With Typescript and Sequelize

## Prerequisites
To run the application, you need to install the following:
- Node
- Postgresql.

## Getting Started
To get started clone the repository into local machine by using the following command:
```
git clone https://gitlab.com/ArunPandianApk/NodeCurrency.git
```
This will create a clone of the repository in the folder NodeCurrencyExchange.

## Running the Application
Running the app involves the following steps:

#### Step 1: Install Dependencies
```
npm install
```

#### Step 2: Run the Application
```
npm run start
```