create table currency (id serial primary key, name varchar(100) not null, value varchar(100) not null);

create table country (id serial primary key, name varchar(100) not null, currency_id int references currency(id));
