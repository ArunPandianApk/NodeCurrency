import * as express  from 'express';
import { currencies } from './routes/currencyRouter';
import { countries } from './routes/countryRouter';
var bodyParser = require('body-parser');
import { serverconfig } from './config';

const server = express();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.use('/api/currencies', currencies);
server.use('/api/countries', countries);

server.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS,   PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Credentials', false);
  next();
});

server.get('/api/', (req, res, next) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(serverconfig.port, serverconfig.host, () => {
  console.log("Server started at http://"+serverconfig.host+":"+serverconfig.port);
});
