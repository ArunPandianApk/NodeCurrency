import { Country } from '../models/countrymodel';
import { Currency } from '../models/currencymodel';
import { sequelize } from '../database';
import { Router } from 'express';
import { Sequelize } from 'sequelize-typescript';

export const countries = Router();
sequelize.addModels([Country]);
sequelize.addModels([Currency]);
const Op = Sequelize.Op;

countries.get('/', (req, res) => {
  Country.findAll({
    attributes: ['id','name'],
    include: [Currency]
  })
  .then((data) => {
    return res.status(200).json(data);
  })
  .catch((err) => {
    res.status(500).send("Error:"+err);
  })
});

countries.get('/:id', async (req, res) => {
  try {
    var country = await Country.scope(req.query['scope'])
    .findAll({
      attributes: ['id', 'name'],
      where:
      {
        id: {
          [Op.eq]: req.params['id']
        }
      },
      include: [Currency]
    });
    res.status(200).json(country);
  }
  catch (err) {
    res.status(500).end('Error:'+err);
  }
});

countries.post('/', async (req, res) => {
  try {
    await Country.create(req.body);
    res.status(201).send('Added Successfully');
  }
  catch (err) {
    res.status(500).end('Error:'+err);
  }
});

countries.put('/:id', async (req, res) => {
  try {
    await Country.update<Country>(req.body, {
      where: {
        id: req.params['id']
      }
    });
    res.sendStatus(200);
  }
  catch (err) {
    res.status(500).end('Error:'+err);
  }
});

countries.delete("/:id", async (req, res) => {
  try {
    await Country.destroy({
      where: {
        id: req.params['id']
      }
    });
    res.status(200).send('Deleted Successfully');
  }
  catch(err){
    res.status(500).end('Error:'+err);
  }
})
