import { Router } from 'express';
import { Country } from '../models/countrymodel';
import { Currency } from '../models/currencymodel';
import { sequelize } from '../database';
import { Sequelize } from 'sequelize-typescript';

export const currencies = Router();
sequelize.addModels([Currency, Country]);
const Op = Sequelize.Op;

currencies.get('/', (req, res) => {
  Currency.findAll({
    include: [Country]
  })
  .then((data) => {
    return res.status(200).json(data);
  })
  .catch((err) => {
    res.status(500).end('Error:'+err);
  })
});

currencies.get('/:id', async (req, res) => {
  try {
    var currency = await Currency.scope(req.query['scope'])
    .findAll({
      where:
      {
        id: {
          [Op.eq]: req.params['id']
        }
      },
      include: [Country]
    });
    res.status(200).json(currency);
  }
  catch (err) {
    res.status(500).end('Error:'+err);
  }
});

currencies.post('/', async (req, res) => {
  try {
    await Currency.create(req.body);
    res.status(201).send('Added Successfully');
  }
  catch (err) {
    res.status(500).end('Error:'+err);
  }
});

currencies.put('/:id', async (req, res) => {
  try {
    await Currency.update<Currency>(req.body, {
      where: {
        id: req.params['id']
      }
    });
    res.sendStatus(200);
  }
  catch (err) {
    res.status(500).end('Error:'+err);
  }
});

currencies.delete("/:id", async (req, res) => {
  try {
    Currency.destroy({
      where: {
        id: req.params['id']
      }
    });
    res.status(200).send('Deleted Successfully');
  }
  catch(err){
    res.status(500).end('Error:'+err);
  }
});
