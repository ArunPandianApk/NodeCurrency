import { Table, AutoIncrement, Column, Model, PrimaryKey, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { Currency } from '../models/currencymodel';

@Table({tableName: 'country'})
export class Country extends Model<Country>
{
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @ForeignKey(() => Currency)
  @Column({field: 'currency_id'})
  currencyId: number;

  @BelongsTo(() => Currency)
  currencies: Currency;
}
