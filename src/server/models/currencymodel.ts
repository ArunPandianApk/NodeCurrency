import {Table, Column, AutoIncrement, Model, PrimaryKey, HasMany} from 'sequelize-typescript';
import { Country } from './countrymodel';

@Table({tableName: 'currency'})
export class Currency extends Model<Currency>
{
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Column
  value: number;

  @HasMany(() => Country)
  country: Country;
}
