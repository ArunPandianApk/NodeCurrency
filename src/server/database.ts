import {Sequelize} from 'sequelize-typescript';
import { dbconfig } from './config';

export const sequelize = new Sequelize(dbconfig);

sequelize.authenticate().then(() => {
  console.log("Connected to DB");
})
.catch((err) => {
  console.log(err);
});
